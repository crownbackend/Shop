<?php

namespace App\Repository;

use App\Entity\RandomMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RandomMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method RandomMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method RandomMessage[]    findAll()
 * @method RandomMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RandomMessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RandomMessage::class);
    }

    // /**
    //  * @return RandomMessage[] Returns an array of RandomMessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RandomMessage
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
