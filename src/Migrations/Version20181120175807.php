<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181120175807 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE command CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE command_line CHANGE product_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `option` CHANGE second_name second_name VARCHAR(255) DEFAULT NULL, CHANGE second_value second_value VARCHAR(255) DEFAULT NULL, CHANGE third_name third_name VARCHAR(255) DEFAULT NULL, CHANGE third_value third_value VARCHAR(255) DEFAULT NULL, CHANGE fourth_name fourth_name VARCHAR(255) DEFAULT NULL, CHANGE fourth_value fourth_value VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product CHANGE options_id options_id INT DEFAULT NULL, CHANGE sale_price sale_price INT DEFAULT NULL, CHANGE promotional_label promotional_label VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE confirmation_token confirmation_token VARCHAR(255) DEFAULT NULL, CHANGE last_login last_login DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE command CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE command_line CHANGE product_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `option` CHANGE second_name second_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE second_value second_value VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE third_name third_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE third_value third_value VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE fourth_name fourth_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE fourth_value fourth_value VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE product CHANGE options_id options_id INT DEFAULT NULL, CHANGE sale_price sale_price INT DEFAULT NULL, CHANGE promotional_label promotional_label VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE confirmation_token confirmation_token VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE last_login last_login DATETIME NOT NULL');
    }
}
