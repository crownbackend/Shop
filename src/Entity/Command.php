<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandRepository")
 */
class Command
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOfOrder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commands")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateOfOrder(): ?\DateTimeInterface
    {
        return $this->dateOfOrder;
    }

    public function setDateOfOrder(\DateTimeInterface $dateOfOrder): self
    {
        $this->dateOfOrder = $dateOfOrder;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
