<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandLineRepository")
 */
class CommandLine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $priceProduct;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantityOrder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="commandLines")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Command")
     * @ORM\JoinColumn(nullable=false)
     */
    private $numberCommand;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPriceProduct()
    {
        return $this->priceProduct;
    }

    public function setPriceProduct($priceProduct): self
    {
        $this->priceProduct = $priceProduct;

        return $this;
    }

    public function getQuantityOrder(): ?int
    {
        return $this->quantityOrder;
    }

    public function setQuantityOrder(int $quantityOrder): self
    {
        $this->quantityOrder = $quantityOrder;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getNumberCommand(): ?Command
    {
        return $this->numberCommand;
    }

    public function setNumberCommand(?Command $numberCommand): self
    {
        $this->numberCommand = $numberCommand;

        return $this;
    }
}
