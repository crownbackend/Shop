<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstImage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secondImage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $thirdImage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fourthImage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="image")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstImage()
    {
        return $this->firstImage;
    }

    public function setFirstImage( $firstImage)
    {
        $this->firstImage = $firstImage;

        return $this;
    }

    public function getSecondImage()
    {
        return $this->secondImage;
    }

    public function setSecondImage( $secondImage)
    {
        $this->secondImage = $secondImage;

        return $this;
    }

    public function getThirdImage()
    {
        return $this->thirdImage;
    }

    public function setThirdImage( $thirdImage)
    {
        $this->thirdImage = $thirdImage;

        return $this;
    }

    public function getFourthImage()
    {
        return $this->fourthImage;
    }

    public function setFourthImage( $fourthImage)
    {
        $this->fourthImage = $fourthImage;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setImage($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getImage() === $this) {
                $product->setImage(null);
            }
        }

        return $this;
    }
}
