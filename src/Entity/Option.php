<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OptionRepository")
 */
class Option
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstValue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondValue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thirdName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thirdValue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fourthName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fourthValue;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="options")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getFirstValue(): ?string
    {
        return $this->firstValue;
    }

    public function setFirstValue(string $firstValue): self
    {
        $this->firstValue = $firstValue;

        return $this;
    }

    public function getSecondName(): ?string
    {
        return $this->secondName;
    }

    public function setSecondName(?string $secondName): self
    {
        $this->secondName = $secondName;

        return $this;
    }

    public function getSecondValue(): ?string
    {
        return $this->secondValue;
    }

    public function setSecondValue(?string $secondValue): self
    {
        $this->secondValue = $secondValue;

        return $this;
    }

    public function getThirdName(): ?string
    {
        return $this->thirdName;
    }

    public function setThirdName(?string $thirdName): self
    {
        $this->thirdName = $thirdName;

        return $this;
    }

    public function getThirdValue(): ?string
    {
        return $this->thirdValue;
    }

    public function setThirdValue(?string $thirdValue): self
    {
        $this->thirdValue = $thirdValue;

        return $this;
    }

    public function getFourthName(): ?string
    {
        return $this->fourthName;
    }

    public function setFourthName(?string $fourthName): self
    {
        $this->fourthName = $fourthName;

        return $this;
    }

    public function getFourthValue(): ?string
    {
        return $this->fourthValue;
    }

    public function setFourthValue(?string $fourthValue): self
    {
        $this->fourthValue = $fourthValue;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setOptions($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getOptions() === $this) {
                $product->setOptions(null);
            }
        }

        return $this;
    }
}
