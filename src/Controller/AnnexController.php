<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
// Front controller this controller manages all the party front of the website
class AnnexController extends Controller
{
    /**
     * @Route("/", name="redirect")
     * @return RedirectResponse
     */
    public function redirectIndex()
    {
        $local = "fr";
        return $this->redirect($this->generateUrl('home.fr', ['_locale' => $local], 302));
    }
}