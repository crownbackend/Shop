<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class RegistrationController contains all the logic of the register
 * @package App\Controller
 * @Route("/{_locale}")
 */
class RegistrationController extends Controller
{
    /**
     * @return string
     * @throws \Exception
     */
    private function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }

    /**
     * @Route({"fr": "/inscription", "en": "/register", "es": "/registro"}, name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param \Swift_Mailer $mailer
     * @return Response $response
     * @throws \Exception
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // generate a new token for confirmation in after register
            $token = $this->generateToken();
            // encode password and set token
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setConfirmationToken($token);
            // get a current email for the user
            $email = $user->getEmail();
            // send mail
            // persist the user
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $id = $user->getId();

            $message = (new \Swift_Message('Confirmation'))
                ->setFrom('admin@cozastore.com')
                ->setTo($email)
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        'emails/confirmations_register.html.twig',
                        [
                            'token' => $token,
                            'id' => $id
                        ]
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }
        return $this->render('register/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/register/{token}/{id}", name="confirmation_register")
     * @param $token
     * @param int $id
     * @return Response
     */
    public function confirmationRegistration($token = null, int $id = null): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $tokenConfirm = $user->getConfirmationToken();

        if($token == $tokenConfirm) {
            $user->setEnabled(1);
            $user->setConfirmationToken('');
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('confirmation', 'Votre compte à bien été activé');
            return $this->redirectToRoute('home');
        } else {
            return new Response('no');
        }
    }

}