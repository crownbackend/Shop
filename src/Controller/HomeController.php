<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * Class HomeController, allows access to the website
 * @package App\Controller
 * @Route("/{_locale}")
 */
class HomeController extends Controller
{

    /**
     * @Route({"fr": "/", "en": "/", "es": "/"
     *     }, name="home")
     */
    public function index()
    {


        return $this->render('Home/index.html.twig');
    }

}